c     PPart_kdist (http://www.meso-star.com/en_Products.html) - This file is part of PPart_kdist
c     Copyright (C) 2015 - Méso-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine exec(command)
      implicit none
      include 'max.inc'
c
c     Purpose: to execute the system command defined in the "command" character string
c
c     Input:
c       + command: character string that defines the system command to execute
c         (example: command='cp toto1 toto2')
c
      integer strlen
      character*(Nchar_mx) command,label
      label='subroutine exec'

      call system(command(1:strlen(command)))

      return
      end
