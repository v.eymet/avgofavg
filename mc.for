      subroutine mc1(Ngroup,Nelem,coeff,elem,Nevent,avg,std_dev)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the average
c     using a naive MC algorithm
c
c     Input:
c       + Ngroup: number of groups
c       + Nelem: number of elements in each group
c       + coeff: coefficient for each group
c       + elem: elements
c       + Nevent: number of statistical events
c
c     Output:
c       + avg: average value of "elem"
c       + std_dev: standard deviation
c
c     I/O
      integer Ngroup
      integer Nelem(1:Ngroup_mx)
      integer coeff(1:Ngroup_mx)
      integer elem(1:Ngroup_mx,1:Nelem_mx)
      integer Nevent
      double precision avg,std_dev
c     temp
      integer tnelem,ielem,igroup
      integer group_idx,elem_idx
      integer event
      double precision contrib
      double precision sum,sum2,variance
c     label
      character*(Nchar_mx) label
      label='subroutine mc1'

c     Total number of elements
      tnelem=0
      do igroup=1,Ngroup
         tnelem=tnelem+Nelem(igroup)*coeff(igroup)
      enddo                     ! igroup
c     Debug
c      write(*,*) 'tnelem=',tnelem
c      stop
c     Debug

c     Initialization
      sum=0.0D+0
      sum2=0.0D+0
      
c     MC loop
      do event=1,Nevent
c     sample element in the [1-tnelem] range
         call choose_integer(1,tnelem,ielem)
c     Debug
c         write(*,*) 'ielem=',ielem
c     Debug
         call identify_indexes(Ngroup,Nelem,coeff,elem,ielem,
     &        group_idx,elem_idx)
c     Debug
c         write(*,*) event,ielem,group_idx,elem_idx
c     Debug
c     get contribution
         contrib=dble(elem(group_idx,elem_idx))
         sum=sum+contrib
         sum2=sum2+contrib**2.0D+0
c     identify index of group and element
      enddo                     ! event
      call statistics(Nevent,sum,sum2,avg,variance,std_dev)

      return
      end
      
      
      
      subroutine mc2(Ngroup,Nelem,coeff,elem,Nevent,avg,std_dev)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the average
c     using a (hopefully) less naive MC algorithm
c
c     Input:
c       + Ngroup: number of groups
c       + Nelem: number of elements in each group
c       + coeff: coefficient for each group
c       + elem: elements
c       + Nevent: number of statistical events
c
c     Output:
c       + avg: average value of "elem"
c       + std_dev: standard deviation
c
c     I/O
      integer Ngroup
      integer Nelem(1:Ngroup_mx)
      integer coeff(1:Ngroup_mx)
      integer elem(1:Ngroup_mx,1:Nelem_mx)
      integer Nevent
      double precision avg,std_dev
c     temp
      integer tnelem
      double precision wgroup(1:Ngroup_mx)
      integer igroup,group_idx,elem_idx
      integer event
      double precision contrib
      double precision sum,sum2,variance
c     label
      character*(Nchar_mx) label
      label='subroutine mc2'

c     compute ponderation weight for each group
      tnelem=0
      do igroup=1,Ngroup
         tnelem=tnelem+Nelem(igroup)*coeff(igroup)
      enddo                     ! igroup
      do igroup=1,Ngroup
         wgroup(igroup)=dble(Nelem(igroup)*coeff(igroup))/dble(tnelem)
      enddo                     ! igroup

c     Initialization
      sum=0.0D+0
      sum2=0.0D+0

c     MC loop
      do event=1,Nevent
c     sample group
         call sample_group(Ngroup,wgroup,group_idx)
c     sample element in group
         call choose_integer(1,Nelem(group_idx),elem_idx)
c     get contribution
         contrib=dble(elem(group_idx,elem_idx))
         sum=sum+contrib
         sum2=sum2+contrib**2.0D+0
c     identify index of group and element
      enddo                     ! event
      call statistics(Nevent,sum,sum2,avg,variance,std_dev)
      

      return
      end



      subroutine sample_group(Ngroup,wgroup,group_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: to sample a group according to the provided weights
c     
c     Input:
c       + Ngroup: number of groups
c       + wgroup: group weights
c     
c     Output:
c       + group_idx: group index
c     
c     I/O
      integer Ngroup
      double precision wgroup(1:Ngroup_mx)
      integer group_idx
c     temp
      double precision R
      double precision sum_w
      integer igroup
      logical gidx_found
c     label
      character*(Nchar_mx) label
      label='subroutine sample_group'

      call random_gen(R)
      gidx_found=.false.
      sum_w=0.0D+0
      do igroup=1,Ngroup
         if ((R.gt.sum_w).and.(R.le.sum_w+wgroup(igroup))) then
            gidx_found=.true.
            group_idx=igroup
            goto 111
         endif
         sum_w=sum_w+wgroup(igroup)
      enddo                     ! igroup
 111  continue
      if (.not.gidx_found) then
         call error(label)
         write(*,*) 'Group index could not be found'
         write(*,*) 'R=',R
         stop
      endif

      return
      end



      subroutine identify_indexes(Ngroup,Nelem,coeff,elem,ielem,
     &     group_idx,elem_idx)
      implicit none
      include 'max.inc'
c     
c     Purpose: identify the indexes for group and element in the
c     "elem" array from a global element index
c
c     Input:
c       + Ngroup: number of groups
c       + Nelem: number of elements in each group
c       + coeff: coefficient for each group
c       + elem: elements
c       + ielem: global element index
c
c     Output:
c       + group_idx: group index in the "elem" array
c       + elem_idx: element index in the "elem" array
c
c     I/O
      integer Ngroup
      integer Nelem(1:Ngroup_mx)
      integer coeff(1:Ngroup_mx)
      integer elem(1:Ngroup_mx,1:Nelem_mx)
      integer ielem
      integer group_idx,elem_idx
c     temp
      integer igroup,jelem0,jelem,kelem,i
      integer tmp1,tmp2
      logical gidx_found,eidx_found
c     label
      character*(Nchar_mx) label
      label='subroutine identify_indexes'

c     Identify group index
      gidx_found=.false.
      tmp1=0
      tmp2=0
      do igroup=1,Ngroup
         tmp2=tmp2+Nelem(igroup)*coeff(igroup)
         if ((ielem.gt.tmp1).and.(ielem.le.tmp2)) then
            gidx_found=.true.
            group_idx=igroup
            jelem0=ielem-tmp1
            goto 111
         endif
         tmp1=tmp2
      enddo                     ! igroup
 111  continue
      if (.not.gidx_found) then
         call error(label)
         write(*,*) 'Group index could not be found'
         write(*,*) 'ielem=',ielem
         write(*,*) 'tmp2=',tmp2
         stop
      endif
c     Debug
c      write(*,*) 'group_idx=',group_idx,' jelem0=',jelem0
c     Debug
c     Identify element index
      eidx_found=.false.
      jelem=jelem0
      do i=1,Nelem(group_idx)
         kelem=jelem-coeff(group_idx)
         if (kelem.le.0) then
            eidx_found=.true.
            elem_idx=i
            goto 222
         endif
         jelem=kelem
      enddo                     ! icoeff
 222  continue
      if (.not.eidx_found) then
         call error(label)
         write(*,*) 'Element index could not be found'
         write(*,*) 'jelem0=',jelem0
         write(*,*) 'Nelem*coeff(',group_idx,')=',
     &        Nelem(group_idx)*coeff(group_idx)
         stop
      endif
c     Check validity
      if ((group_idx.le.0).or.(group_idx.gt.Ngroup)) then
         call error(label)
         write(*,*) 'group_idx=',group_idx
         if (group_idx.le.0) then
            write(*,*) 'should be > 0'
         else
            write(*,*) 'while Ngroup=',Ngroup
         endif
         stop
      endif
      if ((elem_idx.le.0).or.(elem_idx.gt.Nelem(group_idx))) then
         call error(label)
         write(*,*) 'elem_idx=',elem_idx
         if (elem_idx.le.0) then
            write(*,*) 'should be > 0'
         else
            write(*,*) 'while Nelem(',group_idx,')=',Nelem(group_idx)
         endif
      endif

      return
      end
