c     PPart_kdist (http://www.meso-star.com/en_Products.html) - This file is part of PPart_kdist
c     Copyright (C) 2015 - Méso-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      subroutine error(label)
      implicit none
      include 'max.inc'
c
c     Purpose: to display the name of the subroutine the error originates from
c
c     Input:
c       + label: name of the subroutine that generates the error
c

c     I/O
      character*(Nchar_mx) label

      write(*,*) 'Error from ',trim(label),' :'

      return
      end



      subroutine warning(label)
      implicit none
      include 'max.inc'
c
c     Purpose: to display the name of the subroutine the warning originates from
c
c     Input:
c       + label: name of the subroutine that generates the error
c

c     I/O
      character*(Nchar_mx) label

      write(*,*) 'Warning from ',trim(label),' :'

      return
      end
