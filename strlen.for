c     PPart_kdist (http://www.meso-star.com/en_Products.html) - This file is part of PPart_kdist
c     Copyright (C) 2015 - Méso-Star - Vincent Eymet
c     
c     This file must be used under the terms of the CeCILL license.
c     This source file is licensed as described in the file COPYING, which
c     you should have received as part of this distribution.  The terms
c     are also available at
c     http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.txt
c     
      integer function strlen(st)
      integer i
      character st*(*)

      i=len(st)
      if (i.gt.0) then
         do while ((st(i:i).eq.' ').and.(i.gt.0))
            i=i-1
            if (i.eq.0) then
               goto 111
            endif
         enddo
      endif ! i>0
 111  continue
      strlen=i
      return
      end
