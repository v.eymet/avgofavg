      program generate_dataset
      implicit none
      include 'max.inc'
c     variables
      integer seed
      integer Ngroup,igroup
      integer Nelem(1:Ngroup_mx),ielem,elem
      integer coeff(1:Ngroup_mx)
      character*(Nchar_mx) file_out
      integer nelem_tot
      double precision sum,avg
c     label
      character*(Nchar_mx) label
      label='program generate_dataset'

c     Initialization
c     random seed
      seed=1234
c     name of the dataset file
      file_out='./dataset.txt'

      call zufalli(seed)
      call choose_integer(1,Ngroup_mx,Ngroup)
      do igroup=1,Ngroup
         call choose_integer(1,Nelem_mx,Nelem(igroup))
      enddo                     ! igroup

      nelem_tot=0
      sum=0.0D+0
      open(11,file=trim(file_out))
      write(*,*) 'Recording: ',trim(file_out)
      write(11,*) Ngroup
      do igroup=1,Ngroup
         write(11,*) Nelem(igroup)
c     generate coefficient of the group
         call choose_integer(1,10,coeff(igroup))
         write(11,*) coeff(igroup)
         nelem_tot=nelem_tot+Nelem(igroup)*coeff(igroup)
c     generate elements
         do ielem=1,Nelem(igroup)
            if (coeff(igroup).ge.8) then
               call choose_integer(16,20,elem)
            else
               call choose_integer(1,20,elem)
            endif
            write(11,*) elem
            sum=sum+elem*coeff(igroup)
         enddo                  ! ielem
      enddo                     ! igroup
      close(11)
      write(*,*) 'done'
      avg=sum/dble(nelem_tot)
      write(*,*) 'average=',avg

      end
