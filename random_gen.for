      subroutine random_gen(tmp)
      implicit none
      include 'max.inc'
c
c     Main pseudo-random number generator routine
c     (the one that must be called)
c
c     I/O
      double precision tmp
c     temp
      integer n,t
      character*(Nchar_mx) label
      label='subroutine random_gen'

      n=1
      t=0
 10   continue
      call zufall(n,tmp)
      if (tmp.eq.0.or.tmp.eq.1) then
         t=t+1
         if (t.gt.10) then
            call error(label)
            write(*,*) 'Too many zeroes'
            stop
         endif
         goto 10
      endif

      return
      end


      subroutine uniform(val1,val2,alpha)
      implicit none
      include 'param.inc'
      include 'max.inc'
c
c     Purpose: to return a value between "val1" and "val2", chosen uniformally
c
c     I/O
      double precision val1,val2,alpha
c     temp
      double precision R
      character*(Nchar_mx) label
      label='subroutine uniform'

      if (val2.le.val1) then
         call error(label)
         write(*,*) 'val2=',val2
         write(*,*) '< val1=',val1
         stop
      endif
      call random_gen(R)
      alpha=val1+(val2-val1)*R

      return
      end

      

      subroutine choose_integer(N1,N2,i)
      implicit none
      include 'max.inc'
c
c     Purpose: to choose an integer between N1 and N2 (included)
c     on a random uniform basis
c
c     Inputs:
c       + N1: minimum value the integer can take
c       + N2: maximum value the integer can take
c
c     Output:
c       + i: integer value between N1 and N2
c
c     I/O
      integer N1,N2,i
c     temp
      double precision R
c     label
      character*(Nchar_mx) label
      label='subroutine choose_integer'

      call random_gen(R)
      i=N1+int((N2-N1+1)*R)

      return
      end
