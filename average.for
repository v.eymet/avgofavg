      program average
      implicit none
      include 'max.inc'
c     variables
      character*(Nchar_mx) dataset_file
      integer seed
      integer Ngroup
      integer Nelem(1:Ngroup_mx)
      integer coeff(1:Ngroup_mx)
      integer elem(1:Ngroup_mx,1:Nelem_mx)
      integer Nevent
      double precision avg,std_dev
c     label
      character*(Nchar_mx) label
      label='program average'

c     Initializations
      Nevent=100
      seed=1234
      call zufalli(seed)

      dataset_file='./dataset.txt'
      call read_dataset(dataset_file,Ngroup,Nelem,coeff,elem)

      call analytic_avg(Ngroup,Nelem,coeff,elem,avg)
      write(*,*) 'analytic average=',avg

c     naive MC
      call mc1(Ngroup,Nelem,coeff,elem,Nevent,avg,std_dev)
      write(*,*) 'MC1 average=',avg,' +/-',std_dev

c     optimized formulation
      call mc2(Ngroup,Nelem,coeff,elem,Nevent,avg,std_dev)
      write(*,*) 'MC2 average=',avg,' +/-',std_dev

      end
