      subroutine read_dataset(dataset_file,Ngroup,Nelem,coeff,elem)
      implicit none
      include 'max.inc'
c
c     Purpose: to read a dataset file
c
c     Input:
c       + dataset_file: file to read
c
c     Output:
c       + Ngroup: number of groups
c       + Nelem: number of elements in each group
c       + coeff: coefficient for each group
c       + elem: elements
c     
c     I/O
      character*(Nchar_mx) dataset_file
      integer Ngroup
      integer Nelem(1:Ngroup_mx)
      integer coeff(1:Ngroup_mx)
      integer elem(1:Ngroup_mx,1:Nelem_mx)
c     temp
      integer ios
      integer igroup,ielem
c     label
      character*(Nchar_mx) label
      label='subroutine read_dataset'

      open(11,file=trim(dataset_file),status='old',iostat=ios)
      if (ios.ne.0) then        ! file not found
         call error(label)
         write(*,*) 'File not found:'
         write(*,*) trim(dataset_file)
         stop
      else
         write(*,*) 'Reading: ',trim(dataset_file)
      endif
      read(11,*) Ngroup
      if (Ngroup.gt.Ngroup_mx) then
         call error(label)
         write(*,*) 'Ngroup=',Ngroup
         write(*,*) '> Ngroup_mx=',Ngroup_mx
         stop
      endif
      do igroup=1,Ngroup
         read(11,*) Nelem(igroup)
         if (Nelem(igroup).gt.Nelem_mx) then
            call error(label)
            write(*,*) 'Nelem(',igroup,')=',Nelem(igroup)
            write(*,*) '> Nelem_mx=',Nelem_mx
            stop
         endif
         read(11,*) coeff(igroup)
         do ielem=1,Nelem(igroup)
            read(11,*) elem(igroup,ielem)
         enddo                  ! ielem
      enddo                     ! igroup
      close(11)
      write(*,*) 'done'

      return
      end
      
