      subroutine statistics(N,sum,sum2,mean,variance,std_dev)
      implicit none
      include 'max.inc'
c
c     Purpose: to compute the variance and standard deviation
c     of a statistical variable that is computed by the Monte-Carlo statistical method
c
c     Input:
c       + N: number of statistical realizations
c       + sum: sum of the N contributions
c       + sum2: sum of the squares of the N contributions
c
c     Output:
c       + mean: variable expectancy
c       + variance: variable statistical variance
c       + std_dev: variable statistical standard deviation
c

c     I/O
      integer N
      double precision sum,sum2
      double precision mean,variance,std_dev
c     temp
c     label
      character*(Nchar_mx) label
      label='subroutine statistics'

      mean=sum/dble(N)
      variance=dabs(sum2/dble(N)-(sum/dble(N))**2)
      std_dev=dsqrt(variance/dble(N))

      return
      end
