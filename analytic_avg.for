      subroutine analytic_avg(Ngroup,Nelem,coeff,elem,avg)
      implicit none
      include 'max.inc'
c     
c     Purpose: to compute the average (analytic computation)
c     
c     Input:
c       + Ngroup: number of groups
c       + Nelem: number of elements in each group
c       + coeff: coefficient for each group
c       + elem: elements
c
c     Output:
c       + avg: average value of "elem"
c
c     I/O
      integer Ngroup
      integer Nelem(1:Ngroup_mx)
      integer coeff(1:Ngroup_mx)
      integer elem(1:Ngroup_mx,1:Nelem_mx)
      double precision avg
c     temp
      integer igroup,ielem
      double precision sum
      double precision w_group
      integer sum_nw
c     label
      character*(Nchar_mx) label
      label='subroutine analytic_avg'

      write(*,*) 'Computing analytic average...'
      sum_nw=0
      do igroup=1,Ngroup
         sum_nw=sum_nw+Nelem(igroup)*coeff(igroup)
      enddo                     ! igroup

      avg=0.0D+0
      do igroup=1,Ngroup
         w_group=dble(Nelem(igroup)*coeff(igroup))/dble(sum_nw)
         sum=0.0D+0
         do ielem=1,Nelem(igroup)
            sum=sum+dble(elem(igroup,ielem))
         enddo                  ! ielem
         avg=avg+w_group*sum/dble(Nelem(igroup))
      enddo                     ! igroup
      write(*,*) 'done'

      return
      end
