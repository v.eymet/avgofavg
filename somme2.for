      program somme2
      implicit none

      integer seed
      integer i,n
      double precision r(1:100000000)
      double precision somme
      integer Nevent,event
      integer nombre
      double precision contrib
      double precision sum,sum2
      double precision mean,variance,std_dev

      seed=1234
      call zufalli(seed)

      n=100000000
      somme=0.0D+0
      do i=1,n
         call uniform(0.0D+0,20.0D+0,r(i))
         somme=somme+r(i)
      enddo                     ! i
      write(*,*) 'Somme théorique=',somme
      
      Nevent=10000
      sum=0.0D+0
      sum2=0.0D+0
      do event=1,Nevent
         call choose_integer(1,n,i)
         contrib=dble(n)*r(i)
c     Debug
c         write(*,*) contrib
c     Debug
         sum=sum+contrib
         sum2=sum2+contrib**2.0D+0
c     Debug
c         write(*,*) event,nombre,contrib,sum
c     Debug
      enddo                     ! n

      call statistics(Nevent,sum,sum2,mean,variance,std_dev)
      write(*,*) 'mean=',mean,' +/-',std_dev
      write(*,*) 'différence:',std_dev/somme*100.0,' %'

      end
      
